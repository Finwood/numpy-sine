#!/usr/bin/env python3
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt

from tools import msg

with msg("loading pulse data"):
    pulse = np.load('data/pulse.npy')

    pulse_samples = len(pulse)
    pulse_samplerate = 960  # 960 Hz
    pulse_duration = pulse_samples / pulse_samplerate
    pulse_time = np.linspace(0, pulse_duration, pulse_samples, endpoint=False)

plt.figure(figsize=(6, 2.5))
plt.title("pulse signal")
plt.plot(pulse_time, pulse)
plt.savefig('pulse.png')

with msg("loading data"):
    ts = np.load('data/t_full.npy')

plt.figure(figsize=(6, 4))
plt.title("difference between time vectors")
plt.xlabel("samples")
plt.ylabel("difference")
x = np.arange(len(ts[0]))

with msg("plotting"):
    plt.plot(x, ts[0]-ts[1])

with msg("saving figure as png"):
    plt.savefig('plot.png')
