PYTHON = python3
RM = rm -rf

all: plot test

test: data/t.npy
	$(PYTHON) sine.py

plot: plot.png

data/t.npy data/t_full.npy: resample.py data/pulse.npy
	$(PYTHON) resample.py

plot.png: plot.py data/t_full.npy
	$(PYTHON) plot.py

pulse.png: plot.py data/pulse.npy
	$(PYTHON) plot.py

clean:
	$(RM) data/t.npy data/t_full.npy plot.png __pycache__
