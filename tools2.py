# coding: utf-8

from contextlib import contextmanager
import time

@contextmanager
def msg(m, end="done.", timing=True):
    print m + '...',
    t_start = time.time()
    yield
    if timing:
        t = time.time() - t_start
        end = "{s:{d}s}".format(s=end, d=80-len(m)-15)
        if t < 1e-3:
            end += "[{:5.1f} µs]".format(1e6 * t)
        elif t < 1:
            end += "[{:5.1f} ms]".format(1e3 * t)
        else:
            end += "[{:5.1f} s ]".format(t)
    print end
