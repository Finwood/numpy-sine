# coding: utf-8

import sys
from math import log, floor


if sys.version_info >= (3, ):
    from tools3 import *
else:
    from tools2 import *


def user(msg, dtype=str):
    while True:
        try:
            x = input(msg + ": ")
            try:
                return True, dtype(x)
            except ValueError:
                print("{!r} expected, try again.\n".format(dtype))
                continue
        except (KeyboardInterrupt, EOFError):
            return False, None

def hr_val(value, fmt='.0f', sep=' '):
    mag = floor(log(value, 1e3))
    if mag > 4:
        mag = 4
    if mag < -4:
        mag = -4

    prefix = ['', 'k', 'M', 'G', 'T', 'p', 'n', 'µ', 'm']

    fmt_string = '{{0:{}}}{}{}'.format(fmt, sep, prefix[mag])
    return fmt_string.format(value / 1000**mag)
