#!/usr/bin/env python3
# coding: utf-8

import numpy as np

print("Using NumPy " + np.__version__)

from tools import msg


with msg("loading data"):
    t = np.load('data/t.npy')

frequency = 40e6  # 40 MHz
omega_t = 2 * np.pi * frequency * t

with msg("fast cosine"):
    np.cos(omega_t[0])
with msg("fast sine"):
    np.sin(omega_t[0])

with msg("slow cosine"):
    np.cos(omega_t[1])
with msg("slow sine"):
    np.sin(omega_t[1])
