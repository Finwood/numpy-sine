#!/usr/bin/env python3
# coding: utf-8

import numpy as np

from tools import msg, hr_val


print("Using NumPy " + np.__version__)

with msg("loading pulse data"):
    pulse = np.load('data/pulse.npy')

    pulse_samples = len(pulse)
    pulse_samplerate = 960  # 960 Hz
    pulse_duration = pulse_samples / pulse_samplerate
    pulse_time = np.linspace(0, pulse_duration, pulse_samples, endpoint=False)

print("pulse data loaded, {}samples, duration {}s" \
        .format(hr_val(pulse_samples), hr_val(pulse_duration)))

carrier_freq = 40e6  # 40 MHz
carrier_samplerate = 100e6  # 100 MHz
carrier_samples = pulse_duration * carrier_samplerate

with msg("calculating hi-res time vector (linspace)"):
    t1 = np.linspace(0, pulse_duration, carrier_samples)

with msg("calculating hi-res time vector (scipy.signal.resample)"):
    # method used in scipy.signal.resample
    # https://github.com/scipy/scipy/blob/v0.17.0/scipy/signal/signaltools.py#L1754
    t2 = np.arange(0, carrier_samples) * (pulse_time[1] - pulse_time[0]) \
            * pulse_samples / float(carrier_samples) + pulse_time[0]

lim = int(-1e6)

with msg("saving time vectors to disk"):
    np.save('data/t.npy', [t1[lim:], t2[lim:]])
    np.save('data/t_full.npy', [t1, t2])
